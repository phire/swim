//! Utilities for writing tests.

use std::sync::Mutex;

use assert_fs::prelude::*;
use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::Result;
use local_impl::local_impl;
use log::debug;

use crate::config;
use crate::spade;

#[derive(Clone)]
pub(crate) struct SpadeDirs {
    pub(crate) compiler_dir: Utf8PathBuf,
    pub(crate) spade_target: Utf8PathBuf,
}

// We keep one copy of the Spade compiler that can be copied freely to tests that require it,
// instead of compiling it again and again for every test that needs it.
lazy_static::lazy_static! {
    static ref SPADE: Mutex<Option<Option<SpadeDirs>>> = Mutex::new(None);
}

pub(crate) struct Project {
    pub root: assert_fs::TempDir,
    pub root_utf8: Utf8PathBuf,
    pub compiler_dir: Utf8PathBuf,
    pub source_files: Vec<Utf8PathBuf>,
    pub library_dir: Utf8PathBuf,
    pub config: Utf8PathBuf,
    pub lock_file: Utf8PathBuf,
}

impl std::fmt::Debug for Project {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Project {
            compiler_dir,
            config,
            library_dir,
            lock_file,
            root: _,
            root_utf8,
            source_files,
        } = &self;
        f.debug_struct("Project")
            .field("compiler_dir", compiler_dir)
            .field("config", config)
            .field("library_dir", library_dir)
            .field("lock_file", lock_file)
            .field("root", &"..")
            .field("root_utf8", root_utf8)
            .field("source_files", source_files)
            .finish()
    }
}

#[local_impl]
impl TempDirExt for assert_fs::TempDir {
    fn utf8(&self) -> Utf8PathBuf {
        self.to_path_buf().try_into().unwrap()
    }
}

/// Get all files in a directory, recursively.
pub(crate) fn files_in_dir<P: AsRef<Utf8Path>>(path: P) -> Result<Vec<Utf8PathBuf>> {
    let mut files = Vec::new();
    if path.as_ref().is_dir() {
        for entry in path.as_ref().read_dir()? {
            let path: Utf8PathBuf = entry.unwrap().path().try_into()?;
            let mut inner = files_in_dir(&path)?;
            files.append(&mut inner);
        }
    } else {
        files.push(path.as_ref().to_path_buf());
    }
    Ok(files)
}

/// Setup a new swim project (as if running `swim init`).
pub(crate) fn init_repo() -> Result<Project> {
    let root = assert_fs::TempDir::new()?.into_persistent_if(keep_temp_dirs());
    let root_utf8 = root.utf8();
    crate::init::init_in_dir(
        &root_utf8,
        "https://gitlab.com/spade-lang/swim-templates",
        None,
        Some("proj".to_string()),
    )?;
    let project = Project {
        root,
        root_utf8: root_utf8.clone(),
        compiler_dir: root_utf8.join("spade"),
        source_files: vec![root_utf8.join("src").join("main.spade")],
        library_dir: crate::libs_dir(&root_utf8),
        config: root_utf8.join("swim.toml"),
        lock_file: root_utf8.join("swim.lock"),
    };
    // Fetch the compiler source
    spade::update_spade(&root_utf8, &config::Config::read(&root_utf8, &None)?)?;
    Ok(project)
}

// NOTE: This is run before all tests.
#[ctor::ctor]
fn setup_logging() {
    use tracing_subscriber::fmt::TestWriter;
    use tracing_subscriber::prelude::*;
    use tracing_tree::HierarchicalLayer;

    tracing_subscriber::registry()
        .with(
            HierarchicalLayer::new(2)
                .with_targets(true)
                .with_writer(TestWriter::new()),
        )
        .init();
}

fn setup_spade() -> Option<SpadeDirs> {
    debug!("setup_spade");

    if let Ok(path) = std::env::var("SWIM_LOCAL_SPADE") {
        let compiler_dir = Utf8PathBuf::from(path).canonicalize_utf8().unwrap();
        debug!("Using spade at \"{compiler_dir}\" instead of compiling a new one");
        spade::cargo_build(&compiler_dir, spade::BuildTarget::Spade, false, false).unwrap();
        Some(SpadeDirs {
            spade_target: compiler_dir.join("target/release/spade"),
            compiler_dir,
        })
    } else if !env_disabled("SWIM_DOWNLOAD_SPADE") {
        let dir = init_repo().unwrap();
        let _root = dir.root.into_persistent();

        let config = crate::config::Config::new_empty(config::default_compiler());

        let target_dir = spade::build_spade_repository(
            &dir.root_utf8,
            &config,
            spade::BuildTarget::Spade,
            false,
            false,
        )
        .unwrap();
        let spade_target = dir.root_utf8.join("spadec");
        std::fs::copy(target_dir.join("spade"), &spade_target).unwrap();
        std::fs::remove_dir_all(target_dir).unwrap();
        Some(SpadeDirs {
            compiler_dir: crate::compiler_dir(dir.root_utf8, &config.compiler),
            spade_target,
        })
    } else {
        None
    }
}

pub(crate) fn get_compiler() -> SpadeDirs {
    debug!("get_compiler");
    SPADE
        .lock()
        .unwrap()
        .get_or_insert_with(setup_spade)
        .as_ref()
        .expect("Need to either supply a spade compiler by setting environment variable SWIM_LOCAL_SPADE=<path>, or build a new one from source by setting SWIM_DOWNLOAD_SPADE=1")
        .clone()
}

pub(crate) fn init_spade_repo_with(code: &str, config: &str) -> (Project, config::Config) {
    let repo = init_repo().unwrap();
    std::fs::write(repo.root.child("src").child("main.spade"), code).unwrap();
    std::fs::write(&repo.config, config).unwrap();
    let config = config::Config::read(&repo.root_utf8, &None).unwrap();

    (repo, config)
}

pub(crate) fn init_plugin(
    plugin_dir: &Utf8Path,
    config: &str,
    extra_files: Vec<(Utf8PathBuf, &str)>,
) {
    std::fs::create_dir_all(plugin_dir).unwrap();
    git::init(plugin_dir);
    let plugin_config_file = plugin_dir.join("swim_plugin.toml");
    std::fs::write(&plugin_config_file, config).unwrap();
    git::add(plugin_dir, [&plugin_config_file]);
    for (name, content) in extra_files {
        let file = plugin_dir.join(name);
        std::fs::write(&file, content).unwrap();
        git::add(plugin_dir, [&file]);
    }
    git::commit(plugin_dir, "initial commit");
}

/// Check if a environment variable is missing, empty or explicitly set to 0.
fn env_disabled(key: &str) -> bool {
    if let Ok(var) = std::env::var(key) {
        var.is_empty() || var == "0"
    } else {
        true
    }
}

pub(crate) fn enable_spade() -> bool {
    !env_disabled("SWIM_DOWNLOAD_SPADE") || !env_disabled("SWIM_LOCAL_SPADE")
}

pub(crate) fn keep_temp_dirs() -> bool {
    !env_disabled("SWIM_KEEP_TEMP_DIRS")
}

pub(crate) mod git {
    use std::ffi::{OsStr, OsString};
    use std::path::Path;
    use std::process::Command;

    use crate::util::{CommandExt, ExitStatusExt};

    pub(crate) fn add<I>(path: impl AsRef<Path>, files: I)
    where
        I: IntoIterator,
        I::Item: AsRef<Path>,
    {
        let args = ["add", "--force", "--"]
            .iter()
            .map(OsString::from)
            .chain(files.into_iter().map(|p| p.as_ref().into()));
        command(path, args);
    }

    pub(crate) fn commit(path: impl AsRef<Path>, msg: &str) {
        command(path, ["commit", "--allow-empty", "--message", msg, "--"]);
    }

    pub(crate) fn create_branch(path: impl AsRef<Path>, branch: &str) {
        command(path, ["switch", "-c", branch]);
    }

    pub(crate) fn init(path: impl AsRef<Path>) {
        command(path, ["init"])
    }

    pub(crate) fn _checkout_branch(path: impl AsRef<Path>, branch: &str) {
        command(path, ["switch", branch]);
    }

    pub(crate) fn command<I>(path: impl AsRef<Path>, args: I)
    where
        I: IntoIterator,
        I::Item: AsRef<OsStr>,
    {
        Command::new("git")
            .current_dir(path)
            // Unset the environment so we get a "clean" git config.
            .env_clear()
            .args(["-c", "user.name=swim"])
            .args(["-c", "user.email=swim@spade-lang.org"])
            .args(args)
            .log_command()
            .status_and_log_output()
            .unwrap()
            .success_or_else(|| ())
            .unwrap();
    }
}

mod tests {
    use std::collections::HashSet;

    use assert_fs::TempDir;
    use camino::Utf8Path;

    use crate::test::files_in_dir;

    fn setup_compiler_ish_files() -> TempDir {
        // FIXME: into_persistent_if
        // lifeguard: https://gitlab.com/spade-lang/swim/-/merge_requests/117
        let dir = TempDir::new().unwrap();

        let build_dir = dir.join("build");
        let spade_dir = build_dir.join("spade");
        let src_dir = dir.join("src");
        std::fs::create_dir(&build_dir).unwrap();
        std::fs::create_dir(&spade_dir).unwrap();
        std::fs::create_dir(&src_dir).unwrap();
        std::fs::write(dir.join("swim.toml"), "").unwrap();
        std::fs::write(dir.join("pins.pcf"), "").unwrap();
        std::fs::write(build_dir.join("spade.sv"), "").unwrap();
        std::fs::write(spade_dir.join("compiler.rs"), "").unwrap();
        std::fs::write(build_dir.join("hardware.json"), "").unwrap();
        std::fs::write(src_dir.join("main.spade"), "").unwrap();

        dir
    }

    #[test]
    fn clean_does_nothing_without_swim_toml() {
        // FIXME: into_persistent_if
        // lifeguard: https://gitlab.com/spade-lang/swim/-/merge_requests/117
        let dir = assert_fs::TempDir::new().unwrap();
        let dir_utf8 = Utf8Path::from_path(dir.path()).unwrap();

        assert!(crate::cmdline::clean(dir_utf8, false).is_err());
        assert!(crate::cmdline::clean(dir_utf8, true).is_err());
    }

    #[test]
    fn clean_ignores_compiler() {
        let dir = setup_compiler_ish_files();
        let dir_utf8 = Utf8Path::from_path(dir.path()).unwrap();

        let files_before: HashSet<_> = files_in_dir(dir_utf8).unwrap().into_iter().collect();
        crate::cmdline::clean(dir_utf8, false).unwrap();
        let files_after: HashSet<_> = files_in_dir(dir_utf8).unwrap().into_iter().collect();

        dbg!((&files_before, &files_after));

        assert_eq!(
            files_before
                .difference(&files_after)
                .collect::<HashSet<_>>(),
            [
                dir_utf8.join("build/hardware.json"),
                dir_utf8.join("build/spade.sv")
            ]
            .iter()
            .collect(),
        );
    }

    #[test]
    fn clean_all_cleans_compiler_too() {
        let dir = setup_compiler_ish_files();
        let dir_utf8 = Utf8Path::from_path(dir.path()).unwrap();

        let files_before: HashSet<_> = files_in_dir(dir_utf8).unwrap().into_iter().collect();
        crate::cmdline::clean(dir_utf8, true).unwrap();
        let files_after: HashSet<_> = files_in_dir(dir_utf8).unwrap().into_iter().collect();

        dbg!((&files_before, &files_after));

        assert_eq!(
            files_before
                .difference(&files_after)
                .collect::<HashSet<_>>(),
            [
                dir_utf8.join("build/hardware.json"),
                dir_utf8.join("build/spade/compiler.rs"),
                dir_utf8.join("build/spade.sv")
            ]
            .iter()
            .collect(),
        );
    }
}
